# FluxCloud Serverless

## Description

To host stuff like our website, we developed our own cloud because we do not trust the big evil corporations! Of course we use cutting edge technologies, like serverless. Since we know what we are doing, it is totally unhackable. If you want to try, you can check out the demo and if you can access the secret, you will even get a reward :)

Note: This version of the challenge contains a bypass that has been fixed in FluxCloud Serverless 2.0.

## Write-Up

The challenge is a nodejs application and the challenge is to get access to the `/flag` endpoint. There is a custom "Web Application Firewall" that blocks any requests containing the word "flag" (and a few other words, but that's not important now).

As the challenge description noted, there was a very easy bypass for this challenge that I found some way into figuring out the intended solution, but since there's a 2.0 version of this challenge we'll discuss the whole process there.

The unintentional bypass is that the "WAF" only checked for lowercase "flag", but the nodejs routing engine is not case sensitive so it also accepts `/FLAG` for the same route. Our request is not filtered by the WAF now and we get the flag.
