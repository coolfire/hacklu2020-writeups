# Callboy

## Description

Have you ever called a Callboy? No!? Then you should definitely try it. To make it a pleasant experience for you, we have recorded a call with our Callboy to help you get started, so that there is no embarrassing silence between you.

PS: do not forget the to wrap flag{} around the secret

## Write-Up

For this challenge we get a network packet capture. The challenge name and description strongly hint at some VoIP call being in the traffic. To solve this challenge all we need to do is open up the packet capture in Wireshark, in the "Telephony" dropdown menu go to "VoIP calls" and our VoIP stream is automatically found. We can play back the VoIP stream directly from Wireshark and the flag is spoken in the stream.
