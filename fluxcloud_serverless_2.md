# FluxCloud Serverless  2.0

## Description

To host stuff like our website, we developed our own cloud because we do not trust the big evil corporations! Of course we use cutting edge technologies, like serverless. Since we know what we are doing, it is totally unhackable. If you want to try, you can check out the demo and if you can access the secret, you will even get a reward :)

Note: This is the fixed version of FluxCloud Serverless.

## Write-Up

As mentioned for, FluxCloud Serverless 1.0 there was a bypass that was fixed here by making the WAF case insensitive, so our bypass does not work anymore.

After digging into the application for a while it becomes clear there are some "credits" being tracked that would be for billing purposes. The WAF and the main application are billed under separate "accounts", a "team-security" and a "team-products" account are used for the WAF and the main application. (Which has our `/flag` path.) Normally both receive equal credits for any demo deployment, and they are equeally deducted from both accounts for each invocation.

The idea seems to be to make "team-security" run out of credits before "team-products". To see if this would work at all we spun up a local copy of the challenge and changed the cost of for "team-security" from 100 credits per invocation to 1000 per invocation. This seemed to work and when "team-security" credits ran dry we could indeed request `/flag` without the WAF interrupting us.

Now that we had an idea of what we needed to accomplish, we had a few ideas on how we might try to do this:
 - Try to find some method/path that only invokes the WAF but not the main application.
 - Try to make the WAF crash so the ivocation and billing for the WAF is done before the main application is invoked.
 - Find some way to invoke the WAF recursively so its credits would run out in a single invocation.
 - Find some way to make the WAF take a really long time to process so it does not complete within the demo TTL timeout.
 - Try to race the WAF with the exact time the demo TTL expires so the main application would run, but the WAF would time out.
 - (Try to find some other WAF bypass again.)

After a while of probing around I found a method for option 2. The WAF resolves URL encoding to ensure URL encoding cannot be used to bypass the filters and we can abuse this to make the WAF construct an invalid URL out of a previously valid URL. Putting URL encoded UTF-16 characters into the path is perfectly valid, but once the WAF urldecodes them the URL becomes invalid and the WAF crashes. We accomplished this by requesting the path `/fl%c3%61g`. As expected, only "team-security" is billed for this request, so the teams have an inequal amount of credits now. This means we can just request `/flag` until "team-security" credits have run out completely and the WAF no longer blocks the request.
